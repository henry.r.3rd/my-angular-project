// import { Component } from '@angular/core';
//
// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
// })
// export class AppComponent {
//   title = 'angular-app';
// }


// import {component} from '@angular/core';
// import {BrowserModule} from '@angular/platform-browser';
// import {NgModule} from '@angular/core';
// import {FormsMiodule, FormsModule} from '@angular/forms';
// import {ApplicationInitStatus} from './app/component';
//
// @NgModule({
//   declarations:[AppComponent
//   ],
//   imports:[
//     BrowserModule,
//     FormsModule
//   ],
//   providers:[],
//   bootstrap: [ AppComponent ]
//
// })
// export class AppModule {}


import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  form: FormGroup;
  ngOnInit() {
    this.form = new FormGroup({
      patient_name: new FormControl(''),
      patient_age: new FormControl(''),
    });
  }
  onClickSubmit(formData) {
    console.log('Coronal Effected Patient name is : ' + formData.patient_name);
    console.log('Coronal Effected Patient age is : ' + formData.patient_age);
  }
}






