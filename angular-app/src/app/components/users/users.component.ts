import { Component, OnInit } from '@angular/core';
import {User} from "../../models/User";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  displayInfo: boolean = true;

  constructor() { }

  ngOnInit(): void {
    this.users = [
      {
      firstName: 'Bruce',
      lastName: 'Wayne',
      age: 30,
      address: {
        street: '100 Wayne Manor Dr',
        city: 'Gotham City',
        state: 'New York'
      }
    },
      {
        firstName: 'Diana',
        lastName: 'Prince',
        age: 110,
        address: {
          street: '150 N Main St',
          city: 'Los Angles',
          state: 'Califorina'
        }
      }
   ]; // End of the ARRAY

    //Calling our AddUser()
    this.addUser( {
      firstName: 'Jason',
      lastName: 'Todd',
      age: 13
      });

  } // END OF NGONIT () (DO NOT DELETE)


  // Create a method that adds a new user to the array
  addUser(user: User){
    this.users.push(user);
  }

} // END OF CLASS (DO NOT DELETE)
