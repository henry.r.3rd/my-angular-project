import { Component, OnInit } from '@angular/core';
import { Members} from "../../models/Members";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.components.html',
  styleUrls: ['./vip.components.css']
})
export class VipComponent implements OnInit {



  // members: Members[];
  members: Members[];
  displayInfo: boolean = false;
//loading members
//
 loadingMembers: boolean = false;


  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {this.loadingMembers = true},5000)

    this.members = [
      {
        firstName: 'Henry',
        lastName: 'Rodriguez',
        username: 'ImHenry',
        memberNo: 36
      },
      {
        firstName: 'Jon',
        lastName: 'Smith',
        username: 'JonSmith',
        memberNo: 6
      },
      {
        firstName: 'John',
        lastName: 'Doe',
        username: 'Doeboy',
        memberNo: 96
      },
      {
        firstName: 'Johnny',
        lastName: 'Appleseed',
        username: 'seedman',
        memberNo: 26
      },
      {
        firstName: 'JonHenry',
        lastName: 'Henry',
        username: 'strongman',
        memberNo: 17
      }

    ]; //END Of ARRAY


  } // end of ngoninit()

} //END OF CLASS DON'T DELETE!!
