import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
  // PROPERTIES
  num1: number;
  num2: number;

  constructor() {
    this.addNums(4, 2)
  }

  ngOnInit(): void {
  }

//  method
  addNums(x, y) {
    this.num1 = x;
    this.num2 = y;
    console.log(this.num1 + this.num2)

  }

  diffNums(x, y) {
    this.num1 = x;
    this.num2 = y;
    console.log(this.num1 - this.num2)

  }

  productNums(x, y) {
    this.num1 = x;
    this.num2 = y;
    console.log(this.num1 * this.num2)

  }

  quotientNums(x, y) {
    this.num1 = x;
    this.num2 = y;
    console.log(this.num1 / this.num2)

  }



} // end of class (DON'T DELETE)
