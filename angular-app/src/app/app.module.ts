// import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
//
// import { AppRoutingModule } from './app-routing.module';
// import { AppComponent } from './app.component';
//
// @NgModule({
//   declarations: [
//     AppComponent
//   ],
//   imports: [
//     BrowserModule,
//     AppRoutingModule
//   ],
//   providers: [],
//   bootstrap: [AppComponent]
// })
// export class AppModule { }

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HelloComponent } from "./components/hello/hello.component";
import { TravelComponent} from "./components/travel/travel.component";
import { MathComponent } from './components/math/math.component';
import { UsersComponent } from './components/users/users.component';
import { VipComponent} from "./components/vip/vip.components";
import { Vip2Component } from './components/vip2/vip2.component';

@NgModule({
  declarations: [// components go
    AppComponent,
    HelloComponent,
    TravelComponent,
    MathComponent,
    UsersComponent,
    Vip2Component,
    VipComponent
  ],
  imports: [ // modules go here
    // other imports ...
    ReactiveFormsModule,
    BrowserModule,
    FormsModule
  ],
  providers: [], // services go here
  bootstrap: [AppComponent]
})
export class AppModule { }
